\documentclass[utf8]{beamer}
\usepackage[T1]{fontenc}
\usepackage{booktabs}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{caption, subcaption}
 
\usepackage{plex-serif}

\usetheme{metropolis} % e.g. Boadilla, Hannover, metropolis...

\usepackage[backend=bibtex, style=authoryear, natbib=true]{biblatex}

\addbibresource{literatura.bib}
\graphicspath{{./img}}

\newcommand{\LBdots}{\linebreak{} \ldots \linebreak{}}

\title{Graduation Thesis}
\subtitle{Leakage of private information in machine learning models}
\author{Silvije Škudar\\
%{\and} \\ \\ {\textit{Supervisor}: {doc.~dr.~sc.~Ante Đerek}}
}
\institute[FER]{University of Zagreb, Faculty of Electrical Engineering and Computing}
\titlegraphic{\includegraphics[height=1cm]{./fer-logo.png}}
\date{07.07.2022.}

\begin{document}

% Title page
\begin{frame}
    \titlepage
\end{frame}

% Outline / table of contents slide
\begin{frame}
    \frametitle{Table of Contents}
    \tableofcontents
\end{frame}

{
\metroset{sectionpage=none}
\section{Introduction}
}
\begin{frame}
    \frametitle{Introduction}
    \begin{itemize}
        \item Machine Learning as a Service (MLaaS)
            \begin{itemize}
                \item e.g.~Amazon ML, Google Prediction API\ldots
                \item black-box access (unknown training data or models and their parameters)
                \item limited queries
            \end{itemize}
        \item sensitive data
        \item privacy-utility tradeoff
    \end{itemize}
\end{frame}

{
\metroset{sectionpage=none}
\section{Membership Inference Attack}
}
\begin{frame}
    \frametitle{Membership Inference Attack}
    ``Given a machine learning model and a record, determine whether this record was used as part of the model's training dataset or not.'' \citep{shokri2017mia}

    \begin{enumerate}[\(\rightarrow\)]
        \item Can be seen as a binary classification problem.
        \item Various approaches:
            \begin{itemize}
                \item \textbf{Shadow Training (Shokri et al.)}
                \item \textbf{Threshold Attack (Salem et al.)}
                \item \ldots and other variants
            \end{itemize}
        \item Adversarial Knowledge:
            \begin{itemize}
            \item White-box (hyperparameters, training algorithm, gradients\ldots)
            \item \textbf{Black-box} (no knowledge about the model)
            \item \textbf{Attacker might have knowledge about the population}
        \end{itemize}
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{Membership Inference Attack - Shadow Training}
    \begin{columns}
        \column{\dimexpr\paperwidth-10pt}
    \begin{figure}
        \centering
        \begin{subfigure}{0.37\textwidth}
            \centering
                \includegraphics[width=\textwidth]{../doc/img/shadow/slika2-cropped}
        \end{subfigure}
        \hfill
        \begin{subfigure}{0.62\textwidth}
            \centering
                \includegraphics[width=\textwidth]{../doc/img/shadow/slika3-cropped}
        \end{subfigure}
        \caption{Shadow model (left) and attack model (right) training procedures \citep{shokri2017mia}.}
        \label{fig:loadingsMatrix}
    \end{figure}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Membership Inference Attack - Threshold Attack}
    Threshold Attack \citep{salem2018mlleaks}:
    \begin{itemize}
        \item requires no model training nor \textit{shadow data}
        \item \textbf{target} model prediction probabilities are compared to a threshold (above \(\rightarrow\) "in", below \(\rightarrow\) "out")
    \end{itemize}
\end{frame}

\section{Thesis Scope, Assumptions and Setup}
\subsection{Thesis Scope}
\begin{frame}
    \frametitle{Thesis Scope}
    Goal: investigate which factors lead to the possibility of the MIA and its mitigation
    \begin{itemize}
        \item Assumption - overfitting as the main driver:
        \begin{enumerate}[\(\rightarrow\)]
            \item \textbf{reproduction of previous research:}
            \item hypothesis class (NN architectures, various non-NN models)
            \item training algorithm (early stopping, DP-SGD)
            \item availability of training data (training size, \color{gray}per-class size\color{black})
            \item correctly vs.~incorrectly classified examples
        \end{enumerate}
    \item impact of data features (PCA)
        \color{gray}
        \item decision boundaries
        \item \textit{shadow data} synthetisation using CMA-ES
    \end{itemize}
\end{frame}

\subsection{Experimental Setup}
\begin{frame}
    \frametitle{Experimental Setup}
    \begin{itemize}
        \item \textbf{Workflow\footnote{using TensorFlow Privacy \citep{andrew2019tensorflow}}:}
            \begin{enumerate}[\(\rightarrow\)]
                \item train the model
                \item obtain prediction vectors (train and test)
                \item use test data to train shadow and attack models
                \item pass training data predictions to attack model (kNN, RF, TA) - best for estimating leakage from a defensive perspective
            \end{enumerate}
        \item \textbf{Metrics:}
            \begin{itemize}
                \item AUC (and ROC)
                \item Membership risk: \(r(\textbf{z}) = P(\textbf{z} \in \mathcal{D}_{tr}|O(f_\theta, \textbf{z}))\) \citep{song2021systematic}
            \end{itemize}
        \item \textbf{Datasets and Models:}
            \begin{itemize}
                \item Purchase100 - Large and Small NNs (600 features, 100 classes)
                \item UCI Adult - Various non-NN models (13 features, 2 classes)
            \end{itemize}
    \end{itemize}
\end{frame}

\section{Experiments}
\begin{frame}
    \frametitle{Training Size}
    \begin{figure}[ht]
        \centering
        Impact of training set size on attack success
        \includegraphics[width=\textwidth]{../doc/img/purchase-fractions-metrics/purchase-fractions-train-test-aucs}
        \caption{Mean AUC of the MIA against neural networks trained on the Purchase100 dataset. Means are 0.946, 0.897, 0.851, 0.816 for the small architecture and 0.693, 0.662, 0.648, 0.631 for the large one.}
        \label{fig:purchaseFractionsResults}
    \end{figure}
\end{frame}

\begin{frame}
    \fontsize{7pt}{10pt}\selectfont
    \frametitle{Correctly and Incorrectly Classified Instances}
    \begin{table}[ht]
        \input{../doc/tables/adult_correct_incorrect_fractions_aucs}
    \end{table}
\end{frame}

\begin{frame}
    \frametitle{PCA Analysis}
    \begin{figure}[ht]
        \centering
        \begin{subfigure}{0.47\textwidth}
            \centering
                \includegraphics[width=\textwidth]{../doc/img/pca/pcas_all/pca_pc0_pc1}
        \end{subfigure}
        \begin{subfigure}{0.47\textwidth}
            \centering
                \includegraphics[width=\textwidth]{../doc/img/pca/pcas_all/pca_pc4_pc8}
        \end{subfigure}

        \caption{UCI Adult membership risk vs.~principal components.}
        \label{fig:loadingsMatrix}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{PCA Analysis}
    \begin{figure}[ht]
        \centering
        \begin{subfigure}{\textwidth}
            \centering
            \includegraphics[width=\textwidth]{../doc/img/pca/loadings_annot}
        \end{subfigure}
        \caption{UCI Adult PCA loadings matrix.}
        \label{fig:loadingsMatrix}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Conclusion}
    \begin{itemize}
        \item MIA was successful only on heavily-overfitted models (rare in practice)
        \item MIA is much more difficult on correctly classified examples
        \item It is worth considering using Differential Privacy
    \end{itemize}
    \vfill
    \vfill
    \hfill
    Thank you for your attention!
\end{frame}

\begin{frame}[allowframebreaks, noframenumbering]
%\begin{frame}[noframenumbering]
    \frametitle{References}
    \printbibliography
\end{frame}

\end{document}
