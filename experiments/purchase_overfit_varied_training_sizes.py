import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras import backend as K

from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack import membership_inference_attack as mia
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import AttackInputData
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import SlicingSpec
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import AttackType

import matplotlib.pyplot as plt
import numpy as np
import data_utils
import models
import gc

import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from nbfuns import _DEFAULT_FIGSIZE, run_mia_attack


tf.compat.v1.disable_eager_execution()

attack_types = [
                AttackType.THRESHOLD_ATTACK,
                AttackType.RANDOM_FOREST,
                AttackType.K_NEAREST_NEIGHBORS]

#model_weights_path for saving weights
fractions = [0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2]
train_accs = []
test_accs = []

for n in fractions:
    model = models.model_purchase100(small=False)
    model_weights_path="./trained_models/fractions/purchase100-" + str(n) + "-samples.hdf5"

    #X = np.vstack((x_train[:n], x_test[:n]))
    #Y = np.vstack((y_train[:n], y_test[:n]))

    (x_train, y_train), (x_test, y_test), (x_shadow, y_shadow) = data_utils.load_purchase100(shadow_size=n)

    print(x_train.shape)

    checkpoint = ModelCheckpoint(
        model_weights_path,
        monitor='accuracy',
        verbose=0,
        save_best_only=False,
        mode='max'
    )

    early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=2)
    callbacks_list = [checkpoint]

    history = model.fit(
        x_train,
        y_train,
        batch_size=32,
        epochs=50,
        callbacks=callbacks_list,
        verbose=1
    )

    #plt.rcParams["figure.figsize"] = _DEFAULT_FIGSIZE
    #plt.plot(history.history['accuracy'])
    #plt.plot(history.history['val_accuracy'])
    #plt.title('Purchase100 accuracy')
    #plt.ylabel('accuracy')
    #plt.xlabel('epoch')
    #plt.legend(['train', 'test'], loc='upper left')
    #plt.show()
    #plt.savefig("img/purchase100-" + str(n) + "-samples-accuracy.png")

    print("Evaluate on train data")
    train_acc = model.evaluate(x_train, y_train, batch_size=32)
    print("train loss, train acc:", train_acc)
    print("Evaluate on test data")
    test_acc = model.evaluate(x_test, y_test, batch_size=32)
    print("test loss, test acc:", test_acc)

    probs_train = model.predict(x_train)
    probs_test = model.predict(x_test)
    labels_train = np.argmax(y_train, axis=1)
    labels_test = np.argmax(y_test, axis=1)

    print("Running attack for the model trained on " + str(n) + " samples")
    
    train_accs.append(train_acc)
    test_accs.append(test_acc)
    K.clear_session()
    
    try:
        atk_results = run_mia_attack(probs_train, probs_test, labels_train, labels_test, attack_types)
        atk_results.save("pickles/fractions/purchase100-large" + str(n) + "-samples.p")
        print("Successfully performed the attack")
    except:
        print("Error while attacking")
        continue