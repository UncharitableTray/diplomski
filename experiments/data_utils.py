import pandas as pd
import numpy as np
from numpy.random import default_rng

from tensorflow.keras.utils import to_categorical
from keras.utils.data_utils import get_file

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

import os

DATASET_PATH="./datasets"
PURCHASE100_NAME= "dataset_purchase"
PURCHASE100_NUMPY = "purchase100.npz"

RNG_SEED = 3133731337
RNG = default_rng(RNG_SEED)
RNG = default_rng()

def random_record_purchase100():
    return _one_hot_encoded_feature_generator(600)

def randomize_features_purchase100(features: np.ndarray, num_features=10):
    return _one_hot_encoded_feature_randomizer(features, num_features)

def _one_hot_encoded_feature_generator(length: int):
    return RNG.integers(2, size=(length,))
    #print("Generated ", n)

def _one_hot_encoded_feature_randomizer(features: np.ndarray, num_features=10):
    if features is not None:
        clone = np.ndarray.copy(features)
    else:
        #print("Features are none, skipping...")
        clone = random_record_purchase100()
    
    indices = RNG.choice(clone.size, size=num_features)
    clone[indices] = RNG.integers(2, size=num_features)
    return clone

def _data_shuffle_split(x_train, y_train):
    #TODO
    return NotImplementedError()
    
def cifar10_download():
    # download keras-supported datasets
    (x_train, y_train), (x_test, y_test) = tf.keras.datasets.cifar10.load_data()
    assert x_train.shape == (50000, 32, 32, 3)
    assert x_test.shape == (10000, 32, 32, 3)
    assert y_train.shape == (50000, 1)
    assert y_test.shape == (10000, 1)

def cifar100_download():
    (x_train, y_train), (x_test, y_test) = tf.keras.datasets.cifar100.load_data(label_mode="fine")
    assert x_train.shape == (50000, 32, 32, 3)
    assert x_test.shape == (10000, 32, 32, 3)
    assert y_train.shape == (50000, 1)
    assert y_test.shape == (10000, 1)

def load_purchase100(shadow_size=0.75):

    if not os.path.isdir(DATASET_PATH):
        os.makedirs(DATASET_PATH)
        
    DATASET_FILE = os.path.join(DATASET_PATH, PURCHASE100_NAME)
    print(DATASET_FILE)
    if not os.path.isfile(DATASET_FILE):
        print("Dowloading the dataset...")
        urllib.request.urlretrieve("https://www.comp.nus.edu.sg/~reza/files/dataset_purchase.tgz",os.path.join(DATASET_PATH,"tmp.tgz"))
        print("Dataset Dowloaded")
        tar = tarfile.open(os.path.join(DATASET_PATH,"tmp.tgz"))
        tar.extractall(path=DATASET_PATH)
        
        print("reading dataset...")
        data_set =np.genfromtxt(DATASET_FILE,delimiter=",")
        print("finish reading!")
        X = data_set[:,1:].astype(np.float64)
        Y = (data_set[:,0]).astype(np.int32)-1
        np.savez(os.path.join(DATASET_PATH, PURCHASE100_NUMPY), X=X, Y=Y)
        
    data = np.load(os.path.join(DATASET_PATH, PURCHASE100_NUMPY))
    X = data["features"]
    Y = data["labels"]
    len_train =len(X)
    r = np.load("./data_shuffled/random_r_purchase100.npy")
    x_train=X[r]
    y_train=Y[r]

    if shadow_size != 0.0:
        x_train, x_shadow, y_train, y_shadow = train_test_split(
            x_train, y_train, test_size=shadow_size, random_state=1337
        )
    else:
        (x_shadow, y_shadow) = (None, None)

    x_train, x_test, y_train, y_test = train_test_split(
        x_train, y_train, test_size=0.5, random_state=420420
    )

    return (x_train, y_train), (x_test, y_test), (x_shadow, y_shadow)

def UCI_adult(path='UCI.adult.data'):
    
    path = get_file(path, origin='http://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.data')
    f = pd.read_csv(path, header=None)

    # encode the categorical values
    for col in [1,3,5,6,7,8,9,13,14]:
        le = LabelEncoder()
        f[col] = le.fit_transform(f[col].astype('str'))
    
    # normalize the values
    x_range = [i for i in range(14)]
    f[x_range] = f[x_range]/f[x_range].max()

    x = f[x_range].values
    y = f[14].values
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.30, random_state=0)
    return (x_train, y_train), (x_test, y_test)