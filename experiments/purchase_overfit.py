import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from keras.callbacks import ModelCheckpoint, EarlyStopping

from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack import membership_inference_attack as mia
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import AttackInputData
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import SlicingSpec
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import AttackType

from art.attacks.inference.membership_inference import ShadowModels
from art.attacks.inference.membership_inference import MembershipInferenceBlackBox
from art.utils import to_categorical
from art.estimators.classification import TensorFlowV2Classifier
from art.estimators.classification import KerasClassifier
from art.estimators.keras import KerasEstimator

import matplotlib.pyplot as plt
import numpy as np
import data_utils
import models
import gc

tf.compat.v1.disable_eager_execution()

(x_train, y_train), (x_test, y_test), (x_shadow, y_shadow) = data_utils.load_purchase100(shadow_size=0.75)
model = models.model_purchase100(small=True)
model.summary()

#model_weights_path for saving weights
model_weights_path="./trained_models/purchase100-small-50-epochs.hdf5"
try:
    model.load_weights(model_weights_path)
    print("Loaded model weights successfully from file ", model_weights_path)
except:
    # add a checkpoint
    # model_weights_path for saving updates
    # model_weights_path = "./trained_models/purchase100-weights-improvement-{epoch:02d}-{accuracy:.2f}.hdf5"

    # add early stopping
    #early_stopping = tf.keras.callbacks.EarlyStopping(
    #    monitor="val_loss",
    #    min_delta=0,
    #    patience=0,
    #    verbose=0,
    #    mode="auto",
    #    baseline=None,
    #    restore_best_weights=False,
    #)

    checkpoint = ModelCheckpoint(
        model_weights_path,
        monitor='accuracy',
        verbose=1,
        save_best_only=False,
        mode='max'
    )
    callbacks_list = [checkpoint]
    #callbacks_list = [checkpoint, early_stopping]

    X = np.vstack((x_train, x_test))
    Y = np.vstack((y_train, y_test))
    # WARNING: batch_size must be divisible by DPSGD microbatches size (128 by default)
    history = model.fit(
        X,
        Y,
        validation_split=0.5,
        batch_size=32,
        epochs=50,
        callbacks=callbacks_list,
        verbose=1
    )

    plt.rcParams["figure.figsize"] = (10, 8)
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('Purchase100 accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    #plt.show()
    plt.savefig('img/purchase100-small-overfit-train-test-accuracy.png')

finally:
    print("Evaluate on train data")
    results = model.evaluate(x_train, y_train, batch_size=128)
    print("train loss, train acc:", results)
    print("Evaluate on test data")
    results = model.evaluate(x_test, y_test, batch_size=128)
    print("test loss, test acc:", results)

# #tf.compat.v1.disable_eager_execution()
# #atk_template = models.model_purchase100()
# target_model = models.KerasShadowClassifier(
#     model,
#     batch_size=32,
#     epochs=1,
#     callbacks=None
# )
# shadow_models = ShadowModels(
#     target_model, num_shadow_models=3, random_state=222222222
# )

# #try:
#     # trenutno puca zbog one-hot encoded znacajki, treba dopuniti funkcije za to
# #shadow_dataset = shadow_models.generate_synthetic_shadow_dataset(
# #        target_classifier=model,
# #        dataset_size=100,
# #        member_ratio=0.5,
# #        min_confidence=0.1,
# #        max_retries=100,
# #        max_features_randomized=50,
# #        random_record_fn=data_utils.random_record_purchase100,
# #        randomize_features_fn=data_utils.randomize_features_purchase100
# #)
# #except:
# shadow_dataset = shadow_models.generate_shadow_dataset(x_shadow, y_shadow)

# (member_x, member_y, member_predictions), (nonmember_x, nonmember_y, nonmember_predictions) = shadow_dataset

# print([sm.evaluate(x_test, y_test) for sm in shadow_models.get_shadow_models()])




# gc.collect()
# keras.backend.clear_session()

# attack = MembershipInferenceBlackBox(target_model, attack_model_type="rf")
# attack.fit(member_x, member_y, nonmember_x, nonmember_y, member_predictions, nonmember_predictions)

# x_target_train, y_target_train = x_train, y_train
# x_target_test, y_target_test = x_test, y_test

# member_infer = attack.infer(x_target_train, y_target_train)
# nonmember_infer = attack.infer(x_target_test, y_target_test)
# member_acc = np.sum(member_infer) / len(x_target_train)
# nonmember_acc = 1 - np.sum(nonmember_infer) / len(x_target_test)
# acc = (member_acc * len(x_target_train) + nonmember_acc * len(x_target_test)) / (len(x_target_train) + len(x_target_test))
# print('Attack Member Acc:', member_acc)
# print('Attack Non-Member Acc:', nonmember_acc)
# print('Attack Accuracy:', acc)