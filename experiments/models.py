import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow_privacy.privacy.optimizers import dp_optimizer
from tensorflow_privacy.privacy.keras_models.dp_keras_model import DPSequential

from art.estimators.classification import KerasClassifier


import torch
import torchvision
import torch.nn as nn

keras.Sequential.nb_classes = property(lambda self: self.layers[-1].output.shape[1])

def model_purchase100(small=False, use_dpsgd=False):
    if small:
        layers = [
                tf.keras.layers.Dense(128, activation="tanh", name="layer1", input_shape=(600, )),
                tf.keras.layers.Dense(100, activation="softmax", name="layer5"),
        ]
    else:
        layers = [
                tf.keras.layers.Dense(1024, activation="tanh", name="layer1", input_shape=(600, )),
                tf.keras.layers.Dense(512, activation="tanh", name="layer2"),
                tf.keras.layers.Dense(256, activation="tanh", name="layer3"),
                tf.keras.layers.Dense(128, activation="tanh", name="layer4"),
                tf.keras.layers.Dense(100, activation="softmax", name="layer5"),
        ]

    learning_rate = 0.001 
    l2_norm_clip=1.0
    noise_multiplier=1.1

    if use_dpsgd:
        model = DPSequential(
            l2_norm_clip=l2_norm_clip,
            noise_multiplier=noise_multiplier,
            layers=layers
        )
    else:
        model = tf.keras.Sequential(layers=layers)


    optimizer=keras.optimizers.RMSprop(learning_rate=learning_rate)
    loss = tf.keras.losses.CategoricalCrossentropy(from_logits=False)

    model.compile(
        loss=loss,
        optimizer=optimizer,
        metrics=["accuracy"],
    )
    #print(model.nb_classes)
    return model

class KerasShadowClassifier(KerasClassifier):

    def __init__(
        self,
        model: "sklearn.base.BaseEstimator",
        batch_size=32,
        epochs=3,
        callbacks=None
    ) -> "KerasShadowClassifier":
        super().__init__(model=model)
        self._batch_size = batch_size
        self._epochs = epochs
        self._callbacks = callbacks
        self._histories = []
    
    def clone_for_refitting(self) -> "KerasShadowClassifier":
        from tensorflow.keras import layers

        new_model = model_purchase100()
        new_model.set_weights(self.model.get_weights())
        ksc = KerasShadowClassifier(
            new_model, self._batch_size, self._epochs, self._callbacks
        )
        return ksc

    def fit(self, x, y):
        self._histories.append(super().model.fit(
            x,
            y,
            batch_size=self._batch_size,
            epochs=self._epochs,
            callbacks=self._callbacks
        ))
    
    def evaluate(self, x_test, y_test):
        return super().model.evaluate(x_test, y_test)
    
