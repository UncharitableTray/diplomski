import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from keras.callbacks import ModelCheckpoint, EarlyStopping

from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack import membership_inference_attack as mia
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import AttackInputData
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import SlicingSpec
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import AttackType

from art.attacks.inference.membership_inference import ShadowModels
from art.attacks.inference.membership_inference import MembershipInferenceBlackBox
from art.utils import to_categorical
from art.estimators.classification import TensorFlowV2Classifier
from art.estimators.classification import KerasClassifier
from art.estimators.keras import KerasEstimator

import matplotlib.pyplot as plt
import numpy as np
import data_utils
import models
import gc

tf.compat.v1.disable_eager_execution()

(x_train, y_train), (x_test, y_test), (x_shadow, y_shadow) = data_utils.load_purchase100(shadow_size=0.75)
model = models.model_purchase100(small=True, use_dpsgd=True)
model.summary()

#model_weights_path for saving weights
model_weights_path="./trained_models/purchase100-dpsgd-small-50-epochs.hdf5"
try:
    model.load_weights(model_weights_path)
    print("Loaded model weights successfully from file ", model_weights_path)
except:

    checkpoint = ModelCheckpoint(
        model_weights_path,
        monitor='accuracy',
        verbose=1,
        save_best_only=False,
        mode='max'
    )
    callbacks_list = [checkpoint]
    #callbacks_list = [checkpoint, early_stopping]

    X = np.vstack((x_train, x_test))
    Y = np.vstack((y_train, y_test))
    # WARNING: batch_size must be divisible by DPSGD microbatches size (128 by default)
    history = model.fit(
        X,
        Y,
        validation_split=0.5,
        batch_size=32,
        epochs=50,
        callbacks=callbacks_list,
        verbose=1
    )

    plt.rcParams["figure.figsize"] = (10, 8)
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('Purchase100 DP-SGD accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    #plt.show()
    plt.savefig('img/purchase100-small-dpsgd-train-test-accuracy.png')

finally:
    print("Evaluate on train data")
    results = model.evaluate(x_train, y_train, batch_size=128)
    print("train loss, train acc:", results)
    print("Evaluate on test data")
    results = model.evaluate(x_test, y_test, batch_size=128)
    print("test loss, test acc:", results)

    #print("Generate predictions x_test")
    #predictions = model.predict(x_test)[0]
    #print("obtained prediction: ", predictions)
    #print("expected prediction: ", y_test[0])

#tf.compat.v1.disable_eager_execution()
