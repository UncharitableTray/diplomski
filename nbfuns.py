import pickle
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from mpl_toolkits.mplot3d import Axes3D

from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack import data_structures
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack import membership_inference_attack as mia
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack import plotting
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack import privacy_report

from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import AttackInputData
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import SlicingSpec
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import AttackType
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import AttackResults
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import SingleSliceSpec
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import SlicingFeature

from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.dataset_slicing import get_slice

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from keras.callbacks import ModelCheckpoint, EarlyStopping

_DOCUMENT_WIDTH = 441 # truncated decimal: .01773, in pt
#_DEFAULT_FIGSIZE = (12, 8)

# This function was taken directly from https://jwalton.info/Embed-Publication-Matplotlib-Latex/
def set_size(width, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx], idx


def threshold_tpr_fpr(result, value=0.5):
    _, idx = find_nearest(result.roc_curve.thresholds, value)
    res = (result.roc_curve.thresholds[idx],
           result.roc_curve.tpr[idx],
           result.roc_curve.fpr[idx])
    print("For threshold " + str(value) + " found TPR, FPR " + str(np.around(res, decimals=3)))
    return res[0], res[1], res[2]


_DEFAULT_FIGSIZE = figsize=set_size(_DOCUMENT_WIDTH)


# IBM DP Library Utility
def pickle_privacy_utility_tradeoff(X_train, y_train, X_test, y_test, use_dprf=False):
    import diffprivlib as dp
    """Save privacy-utility tradeoff data to a pickle file. Function saves the dependency of accuracy on epsilons."""
    accuracy = []
    epsilons = np.logspace(-3, 1, 10)

    for eps in epsilons:
        if use_dprf:
            from diffprivlib.models import RandomForestClassifier
            dp_clf = RandomForestClassifier(epsilon=eps, n_estimators=100, random_state=1337, cat_feature_threshold=1, n_jobs=6)
        else:
            dp_clf = dp.models.LogisticRegression(epsilon=eps, data_norm=100)

        dp_clf.fit(X_train, y_train)
        accuracy.append(dp_clf.score(X_test, y_test))
    
    return accuracy, epsilons


def plot_privacy_utility_from_pickle(path=None, name="model"):
    """Plots privacy-utility graph from pickle"""
    epsilons, baseline, accuracy = pickle.load(open(path, "rb"))

    plt.semilogx(epsilons, accuracy, label="Differentially private")
    plt.figure(figsize=_DEFAULT_FIGSIZE)
    plt.plot(epsilons, np.ones_like(epsilons) * baseline, dashes=[2,2], label="Non-private")
    plt.title("Differentially private logistic regression accuracy")
    plt.xlabel("epsilon")
    plt.ylabel("Accuracy")
    plt.ylim(0, 1)
    plt.xlim(epsilons[0], epsilons[-1])
    plt.legend(loc=3)
    plt.savefig("./img/privacy-utility-" + name)


def run_mia_attack(probs_train, probs_test, labels_train, labels_test, attack_types, input=None):
    """Runs a MIA attack using TF Privacy on target data using given attack types."""
    if input is None:
        input = AttackInputData(
            probs_train=probs_train,
            probs_test=probs_test,
            labels_train = labels_train,
            labels_test = labels_test
        )

    # Run several attacks for different data slices
    attacks_result = mia.run_attacks(input,
                                     SlicingSpec(
                                         entire_dataset = True,
                                         by_class = True,
                                         by_classification_correctness = True
                                     ),
                                     attack_types=attack_types,
                                     )

    # Plot the ROC curve of the best classifier
    fig = plotting.plot_roc_curve(
        attacks_result.get_result_with_max_auc().roc_curve)

    # Print a user-friendly summary of the attacks
    print(attacks_result.summary(by_slices = True))
    return attacks_result


def evaluate_and_plot_attack_success(atk_results):
    """Generates a summary and plots for an attack result file."""

    atk_df = atk_results.calculate_pd_dataframe()
    
    print(atk_results.summary(by_slices=True))
    
    plotting.plot_roc_curve(atk_results.get_result_with_max_auc().roc_curve)

    atk_df.drop(atk_df[atk_df["slice feature"] == "Entire dataset"].index, inplace=True)
    atk_df.drop(atk_df[atk_df["slice feature"] == "correctly_classified"].index, inplace=True)

    attack_types = atk_df["attack type"].unique()
    for atk_type in attack_types:
        res = atk_df.loc[atk_df["attack type"] == atk_type]
        res.plot(
            x="slice value",
            y=["AUC", "Attacker advantage"],
            kind="bar",
            layout=(1,2),
            xlabel="Class",
            title="Attack success: " + atk_type,
        )
    return atk_df


def calculate_membership_probabilities(probs_train, probs_test, labels_train, labels_test):
    # compute membership probabilities on all given data slices
    input = AttackInputData(
        probs_train=probs_train,
        probs_test=probs_test,
        labels_train=labels_train,
        labels_test=labels_test
    )

    membership_probability_results = mia.run_membership_probability_analysis(input,
                                                                             SlicingSpec(
                                                                                 entire_dataset = True,
                                                                                 by_class = True,
                                                                                 by_classification_correctness = True))
    # print the summary of membership probability analysis
    print(membership_probability_results.summary(threshold_list=[1, 0.9, 0.8, 0.7, 0.6, 0.5]))
    return membership_probability_results


def plot_class_membership_probabilities_with_luminance(probs_train, probs_test, labels_train, labels_test, X_train, y_train, use_tsne=False, luminance_colormap="flare"):
    class_list = np.unique(y_train)

    input = AttackInputData(
        probs_train=probs_train,
        probs_test=probs_test,
        labels_train=labels_train,
        labels_test=labels_test
    )

    xs = np.array([]).reshape(0,13)
    ps = []
    for c in class_list:
        print('Samples with highest membership risk in class ' + str(c))
        class_slice_spec = SingleSliceSpec(SlicingFeature.CLASS, c)
        class_input_slice = get_slice(input, class_slice_spec)
        class_dataset_idx = np.argwhere(labels_train==c).flatten()

        class_train_membership_probs = mia._compute_membership_probability(class_input_slice).train_membership_probs
        rounded_probs = np.around(class_train_membership_probs, decimals=2)
        #print(X_train[class_dataset_idx].shape)
        xs = np.vstack((xs, X_train[class_dataset_idx]))
        ps.extend(rounded_probs)

    ps = np.array(ps)
    inds = np.argsort(ps)
    inds_rev = inds[::-1]
    if use_tsne:
        plot_tsne_labeled(xs[inds_rev], ps[inds_rev], use_pca=True, seed=31337, use_luminance=True, luminance_colormap=luminance_colormap)
    else:
        plot_pca_labeled(xs[inds_rev], ps[inds_rev], use_luminance=True, luminance_colormap=luminance_colormap)


def plot_membership_probabilities(probs_train, probs_test, labels_train, labels_test, X_train, y_train, num_samples=10, use_tsne=False):
    class_list = np.unique(y_train)

    input = AttackInputData(
        probs_train=probs_train,
        probs_test=probs_test,
        labels_train=labels_train,
        labels_test=labels_test
    )

    for c in class_list:
        print('Samples with highest membership risk in class ' + str(c))
        class_slice_spec = SingleSliceSpec(SlicingFeature.CLASS, c)
        class_input_slice = get_slice(input, class_slice_spec)
        class_dataset_idx = np.argwhere(labels_train==c).flatten()

        class_train_membership_probs = mia._compute_membership_probability(class_input_slice).train_membership_probs

        class_high_risky_idx = np.argsort(class_train_membership_probs)[::-1][:num_samples]
        class_low_risky_idx = np.argsort(np.absolute(class_train_membership_probs-0.5))[:num_samples]

        high_risky_images = X_train[class_dataset_idx[class_high_risky_idx]]
        low_risky_images = X_train[class_dataset_idx[class_low_risky_idx]]

        samples = X_train.tolist() + high_risky_images.tolist() + low_risky_images.tolist()
        labels = labels_train.tolist() + ["highest risk"] * len(high_risky_images) + ["lowest risk"] * len(low_risky_images)

        if use_tsne:
            plot_tsne_labeled(samples, labels, use_pca=True, seed=31337)
        else:
            plot_pca_labeled(samples, labels)


def plot_pca_component_dependency_on_membership_risk(
    probs_train,
    probs_test,
    labels_train,
    labels_test,
    X_train,
    y_train,
    n_components=None,
    seed=None,
    component_indices=None
):
    class_list = np.unique(y_train)

    input = AttackInputData(
        probs_train=probs_train,
        probs_test=probs_test,
        labels_train=labels_train,
        labels_test=labels_test
    )

    pca, pca_res = _pca_scale_transform(X_train)

    if n_components is None:
        n_components = pca.components_.shape[0]

    if component_indices is None:
        component_indices=range(n_components)

    for c in class_list:
        #print('Samples with highest membership risk in class ' + str(c))
        class_slice_spec = SingleSliceSpec(SlicingFeature.CLASS, c)
        class_input_slice = get_slice(input, class_slice_spec)
        class_dataset_idx = np.argwhere(labels_train==c).flatten()

        class_train_membership_probs = mia._compute_membership_probability(class_input_slice).train_membership_probs
        rounded_probs = np.around(class_train_membership_probs, decimals=2)

        for component in component_indices:
            #print(pca_res[:,component].shape)
            plt.figure(figsize=_DEFAULT_FIGSIZE)
            plt.scatter(pca_res[:,component], class_train_membership_probs)
            plt.ylabel("Membership probability")
            plt.xlabel("PC" + str(component))
            plt.title("Membership probability vs. PCA component value for class " + str(c))
            plt.show()


def _scale_data_standard(data):
    scaler = StandardScaler()
    scaler.fit(data)
    scaled = scaler.transform(data)
    return scaled


def _pca_scale_transform(data, n_components=None, seed=None):
    
    pca = PCA(n_components, random_state=seed)
    pca_res = pca.fit_transform(_scale_data_standard(data))
    return pca, pca_res


def plot_tsne_labeled(data, labels, n_components=2, verbose=1, perplexity=40, n_iter=1000, legend="full", seed=None, use_pca=False, use_luminance=False, luminance_colormap="rocket"):
    """Plots tSNE embeddings for labeled data"""

    import time

    scaled = _scale_data_standard(data)
    
    if use_pca:
        pca = PCA(n_components, random_state=seed)
        data = pca.fit_transform(scaled)
    
    time_start = time.time()
    tsne = TSNE(n_components=n_components, verbose=verbose, perplexity=perplexity, n_iter=n_iter, random_state=seed)
    tsne_res = tsne.fit_transform(scaled)
    print('t-SNE done! Time elapsed: {} seconds'.format(time.time()-time_start))

    plt.figure(figsize=_DEFAULT_FIGSIZE)
    n_colors = len(np.unique(labels, return_counts=False))

    if use_luminance:
        palette = sns.color_palette(luminance_colormap, as_cmap=True)
    else:
        palette = sns.color_palette("hls", n_colors)

    ax = sns.scatterplot(
        x=tsne_res[:,0], y=tsne_res[:,1],
        hue=labels,
        palette=palette,
        data=tsne_res,
        legend=legend,
        alpha=0.9
    )
    ax.set_title("t-SNE embeddings")
    plt.show()
    return tsne, tsne_res


def plot_pca_labeled(data, labels, n_components=13, legend="full", seed=None, use_luminance=False, luminance_colormap="rocket"):
    """Plots PCA transformation."""
    
    pca, pca_res = _pca_scale_transform(data, n_components=n_components, seed=seed)

    n_colors = len(np.unique(labels, return_counts=False))
    plt.figure(figsize=_DEFAULT_FIGSIZE)

    if use_luminance:
        palette = sns.color_palette(luminance_colormap, as_cmap=True)
    else:
        palette = sns.color_palette("hls", n_colors)

    for i in range(0, 12):
        for j in range(i+1, 13):
            plt.figure(figsize=_DEFAULT_FIGSIZE)
            ax = sns.scatterplot(
                x=pca_res[:,i], y=pca_res[:,j],
                hue=labels,
                palette=palette,
                data=pca_res,
                legend="full",
                alpha=0.9,
            )
            plt.xlabel("PC" + str(i))
            plt.ylabel("PC" + str(j))
            plt.title("PCA transform")
            name = "pc" + str(i) + "_pc" + str(j) + ".png"
            plt.savefig("./img/pcas_maj/pca_" + name, bbox_inches="tight")
            #plt.show()
    return pca, pca_res


def plot_explained_variance_per_component(data, n_components=None):
    pca, pca_res = _pca_scale_transform(data, n_components)

    plt.figure(figsize=_DEFAULT_FIGSIZE)
    ax = plt.plot(
        range(pca.components_.shape[0]),
        pca.explained_variance_ratio_,
    )
    #plt.title("Explained variance ratio per component")
    plt.xlabel("Components")
    plt.ylabel("Explained variance ratio")
    plt.xticks(range(pca.components_.shape[0]))
    plt.grid(visible=True)
    plt.savefig("./img/pca_explained_variance.pdf", bbox_inches="tight")
    plt.show()
    
    return pca, pca_res


def plot_scree_graph(data, n_components=None):
    pca, pca_res = _pca_scale_transform(data, n_components)

    plt.figure(figsize=_DEFAULT_FIGSIZE)
    ax = plt.plot(
        range(pca.components_.shape[0]),
        pca.explained_variance_,
    )
    #plt.title("Scree graph")
    plt.xlabel("Eigenvectors")
    plt.ylabel("Eigenvalues")
    plt.xticks(range(pca.components_.shape[0]))
    plt.grid(visible=True)
    plt.savefig("./img/pca_scree.pdf", bbox_inches="tight")
    plt.show()
    
    return pca, pca_res


def plot_explained_variance_inverted(data, n_components=None):
    pca, pca_res = _pca_scale_transform(data, n_components)

    sum = []
    last = 0
    for i, curr_v in enumerate(pca.explained_variance_ratio_):
        last = last + curr_v
        sum.append(last)

    plt.figure(figsize=_DEFAULT_FIGSIZE)
    ax = plt.plot(
        range(pca.components_.shape[0]),
        sum,
    )
    plt.axhline(y=0.9, color="r", linestyle="dashed")
    #plt.title("Explained variance ratio per component")
    plt.xlabel("Eigenvectors (components)")
    plt.ylabel("Proportion of explained variance")
    plt.xticks(range(pca.components_.shape[0]))
    plt.grid(visible=True)
    plt.savefig("./img/pca_explained_variance_inverted.pdf", bbox_inches="tight")
    plt.show()
    
    return pca, pca_res


def plot_pca_loadings_heatmap_from_dataframe(df, pca, loadings_matrix=False, annot=False, figsize=None):
    
    comp_names = []
    for i in range(pca.components_.shape[0]):
        comp_names.append("PC" + str(i))

    if loadings_matrix:
        loadings = pca.components_.T * np.sqrt(pca.explained_variance_)
        loading_matrix = pd.DataFrame(loadings, columns=comp_names, index=df.columns)
        plot_data = loading_matrix
        title = "PCA loading matrix"
    else:
        loadings = pd.DataFrame(pca.components_.T, columns=comp_names, index=df.columns)
        plot_data = loadings
        title = "PCA loadings"
    
    if figsize is not None:
        fig, ax = plt.subplots(figsize=figsize)
    else:
        fig, ax = plt.subplots(figsize=_DEFAULT_FIGSIZE)
    
    sns.heatmap(
        plot_data.round(1),
        annot=annot,
        #center=0.0,
        cmap=sns.color_palette("mako", as_cmap=True)
    )
    plt.title(title)
    plt.savefig("./img/loadings.pdf", bbox_inches="tight")
    plt.show()
    
    return plot_data


def plot_pca_binary_3d_with_dataframe(df, n_components=3, legend="full", seed=None, scale=False):
    from matplotlib.colors import ListedColormap
    
    data = df.drop(labels=["index", "income"], axis=1, inplace=False)
    labels = ["0", "1"]
    
    if scale:
        scaler = StandardScaler()
        scaler.fit(data)
        data = scaler.transform(data)
    
    pca = PCA(n_components, random_state=seed)
    pca_res = pca.fit_transform(data)
    print('Explained variation per principal component: {}'.format(pca.explained_variance_ratio_))    

    zeros = df[df["income"] == 0].index.to_numpy()
    ones = df[df["income"] == 1].index.to_numpy()
    
    fig = plt.figure(figsize=_DEFAULT_FIGSIZE)
    ax = fig.add_subplot(111, projection='3d')
    cmap = ListedColormap(sns.color_palette("hls", 2))
    
    print(pca_res[zeros].shape)
    ax.scatter(pca_res[zeros][:,0], pca_res[zeros][:,1], pca_res[zeros][:,2], cmap=cmap, s=50)
    ax.scatter(pca_res[ones][:,0], pca_res[ones][:,1], pca_res[ones][:,2], cmap=cmap, s=50)

    ax.legend(labels)

    if scale:
        sc = " "
    else:
        sc = " no "
        
    ax.set_title("PCA transform of majority (0) and minority (1) classes (with" + sc + "prior scaling)")
    plt.show()


def plot_tsne_binary_3d_with_dataframe(df, n_components=3, verbose=1, perplexity=40, n_iter=1000, legend="full", seed=None, scale=False, use_pca=False):
    from matplotlib.colors import ListedColormap
    import time
    
    data = df.drop(labels=["index", "income"], axis=1, inplace=False)
    labels = ["0", "1"]
    
    if scale:
        data = _scale_data_standard(data)
    
    if use_pca:
        pca = PCA(n_components, random_state=seed)
        data = pca.fit_transform(data)
    
    time_start = time.time()
    tsne = TSNE(n_components=n_components, verbose=verbose, perplexity=perplexity, n_iter=n_iter, random_state=seed)
    tsne_res = tsne.fit_transform(data)
    print('t-SNE done! Time elapsed: {} seconds'.format(time.time()-time_start))

    zeros = df[df["income"] == 0].index.to_numpy()
    ones = df[df["income"] == 1].index.to_numpy()
    
    fig = plt.figure(figsize=_DEFAULT_FIGSIZE)
    ax = fig.add_subplot(111, projection='3d')
    cmap = ListedColormap(sns.color_palette("hls", 2))
    
    #print(pca_res[zeros].shape)
    ax.scatter(tsne_res[zeros][:,0], tsne_res[zeros][:,1], tsne_res[zeros][:,2], cmap=cmap, s=50)
    ax.scatter(tsne_res[ones][:,0], tsne_res[ones][:,1], tsne_res[ones][:,2], cmap=cmap, s=50)

    ax.legend(labels)

    if scale:
        sc = " "
    else:
        sc = " no "
        
    ax.set_title("tSNE embeddings of majority (0) and minority (1) classes (with" + sc + "prior scaling)")
    plt.show()
