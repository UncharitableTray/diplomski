Machine learning models have recently become very powerful tools used to solve various problems across many different domains. A major contributor to their success is the availability of large amounts of data \cite{bengio2021deep} which often contains sensitive information, including browsing histories, medical records and geolocation data among others.

Recent research has shown that some models memorise the sensitive data they are trained on \cite{shokri2017mia, carlini2019secret}, and when they are given to other users as a service, this data can under specific circumstances be extracted by a malicious user. Subsequently, many researchers have investigated the circumstances which lead to such privacy leakage \cite{hu2021survey}, looking at many different aspects such as the model architecture \cite{shokri2017mia,
truex2019demystifying} and its connection to overfitting \cite{yeom2018privacy, rezaei2021difficulty}, statistical properties of
training data
\cite{truex2019demystifying, song2021systematic} and the nature of the model task itself \cite{shokri2017mia, truex2019demystifying}, as well as the potential mitigation mechanisms and their impact on model utility \cite{abadi2016deep, nasr2018advreg, jia2019memguard, jordon2018pate}

The goal of this thesis was to investigate machine learning privacy leakage and its driving factors. Specifically, we looked into if and how model architecture, overfitting, differential privacy and training dataset properties impact privacy leakage. To this end, in this thesis we have done the following:

\begin{inparaenum}[(i)]
\item we used the original membership inference attack (MIA) proposed by Shokri et al.~\cite{shokri2017mia} to replicate their attack experiments and results on Purchase100 \cite{shokri2017mia} and UCI Adult \cite{uciRepository} datasets and a number of different models which they have used. We have replicated their finding that the overfitted models are quite vulnerable using their architecture and the architecture used in \cite{song2021systematic}.

    \item We have also included the threshold attack by Salem et al.~\cite{salem2018mlleaks} to successfully reproduce their experiments and results on Purchase100 and UCI Adult and the aforementioned models. Just like in their experiment, the threshold attack here performed almost as well as the shadow training attack.

    \item Furthermore, we replicated the findings of Rezaei et al.~\cite{rezaei2021difficulty} by performing their experiment on correctly and incorrectly classified samples using the two listed datasets and again demonstrated that the MIA is much more difficult on correctly classified samples.

    \item We also tried to look into the role which decision boundaries play in the MIA by plotting them, but we could not conclude anything due to the nature of the experiment and the obtained results.

    \item We used the privacy risk metric by proposed by Song et al.~\cite{song2021systematic} to measure the privacy risk of individual examples on the random forest trained on the UCI Adult dataset. Inspired by their experiment with the Texas100 experiment using t-SNE embeddings to look at in-class variance for most and least vulnerable classes, we transformed the training set using PCA and plotted the privacy risks against pairs of principal components in search of patterns. We went further, however, and we attempted to correlate the data features to the privacy risk via the principal component loadings matrix.

    \item Finally, we attempted to synthesise our own shadow set for the Purchase100 dataset using CMA-ES, albeit unsuccessfully.
\end{inparaenum}

\noindent
To sum up, we have successfully replicated a number of existing experiments using the aforementioned methods, and we have, to the best of our knowledge, demonstrated something new with our PCA analysis of the privacy risk respective to principal components and individual features and the two unsuccessful experiments with decision boundaries and CMA-ES synthetisation.

The rest of the work is organised as follows: Chapter \ref{ch:background} reviews the existing literature, introduces the necessary notations and theoretical background, drawing heavily on the main research just
referenced. Chapter \ref{ch:methods} presents the goals,
assumptions and structure of our experiments. Chapter \ref{ch:results} presents our results and discusses them in more detail.
