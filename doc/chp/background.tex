\label{ch:background}

Even though we assume that the reader of this thesis is already familiar with the basic principles of machine learning, we still briefly introduce the fundamental concepts and their notation in this section to help clear up any potential ambiguities, as well as make formalising the problem of privacy in machine learning easier. We then give an overview of existing attacks on privacy, which are present at
the time of writing in Section \ref{sec:privacyattacks}.
Afterwards, Sections \ref{sec:mia} and \ref{sec:metrics} give a more detailed account of the membership inference attacks using shadow training and the threshold attack specifically, as well as the metrics for the success of the membership inference attack, and finally, Section \ref{sec:dp} gives a brief overview of proposed frameworks and methods of preventing privacy leakage in machine learning.

\section{Supervised Learning}
\label{sec:supervised}
In \cite{alpaydin2020ml} we find a fairly brief definition of machine learning: ``Machine learning is programming computers to optimize a performance criterion
using example data or past experience. We have a model defined up to some parameters, and learning is the execution of a computer program to optimize the parameters of the model using the training data or past experience''. The ``experience'' of a machine learning algorithm is generally the dataset, which is simply a collection of many examples\footnote{Also called instances. In this thesis, we use the terms equivalently.} \cite{alpaydin2020ml, goodfellow2016deep}. An example is typically a vector \(\textbf{x} \in \mathbb{R}^{n}\) of
\(n\) features \(x_i\) that in some way represent some entity or event \cite{goodfellow2016deep}. We denote the model's parameters as \(\theta\).

While there are many tasks for machine learning, in this thesis we focus on classification where the computer program is asked to specify which of \textit{k} categories some input \(\textbf{x}\) belongs to \cite{goodfellow2016deep}. If the learning algorithm experiences a dataset where each example also is associated with a label or a target during its training procedure, it is then a supervised learning algorithm. In unsupervised learning, however, only input data without labels is
provided and the goal is to find regularities in the input \cite{alpaydin2020ml}. In this thesis, we focus on supervised classifiers specifically. The dataset of \(N\) instances can then be represented as \(\mathcal{X} = \{\textbf{x}^t, y^t\}_{t = 1}^{N}\), where y is the associated label formalised as a  k-dimensional binary vector in which exactly one corresponding dimension is 1 and all others are 0. Finally, the model can then be formalised as a function
\(f_\theta : \mathbb{R}^n \rightarrow \{1, \ldots, k\}\) if it merely outputs the instance's class or in the case of probabilistic classifiers as \(f_\theta : \mathbb{R}^n \rightarrow \mathbb{R}^k\) if we are rather interested in the probabilities of the instance's belonging to each of the classes. It is the latter formalisation that we will be more interested in, since the membership inference attack -- our main area of focus -- relies on such output heavily. 

For the same reason, it is also important to discuss underfitting and overfitting. Underfitting occurs when the model is unable to obtain a sufficiently low error value on the training set, while overfitting happens when the gap between the training error and test error (also known as the generalisation gap) is too large \cite{goodfellow2016deep}. These two occurrences are strongly related to the model's \textit{capacity}, that is, its ability to fit
a wide variety of functions. A model with low capacity will have a hard time fitting the training set, while having a capacity which is too high can lead do overfitting by memorising properties of the training set which do not help in generalising \cite{goodfellow2016deep}. Capacity can be altered by changing the model's \textit{hypothesis space} \cite{goodfellow2016deep} (also known as the \textit{hypothesis class} \cite{alpaydin2020ml}) or the neural network architecture, for example.
It should be clear that capacity, overfitting and underfitting are all directly related to memorisation of training data \cite{carlini2019secret, shokri2017mia, truex2019demystifying}, and our main intuition behind the privacy leakage in machine learning models builds upon these concepts.

\subsection{Classifier Performance}
\label{sec:classifierPerformance}
There is a wide variety of measures for classifiers and a complete overview is outside of the scope of this thesis, so we only present the ones which we either used or discussed subsequently. For binary classifiers, there are four possible cases when classifying an example (shown in what is known as a \textit{confusion matrix} in Table \ref{tab:confusionMatrix}). A \textit{positive} example can either be classified as a positive (therefore, true positive) or as a negative (false negative).
Similarly, a \textit{negative} example can be classified either as a
positive (therefore, false positive) or as a negative (true negative) \cite{alpaydin2020ml}. 

\begin{table}[t]
    \centering
    \begin{tabular}[t]{l l l c}
        \toprule
        True Class & \multicolumn{3}{c}{Predicted Class} \\\midrule
        -& \textbf{Positive} & \textbf{Negative} & Total \\\cmidrule{2-4}
        \textbf{Positive} & TP : true pos & FN : false neg & p \\\cmidrule{2-4}
        \textbf{Negative} & FP : false pos & TN : true neg & n \\\midrule
        Total & p' & n' & N\\
        \bottomrule
    \end{tabular}
    \caption{Confusion matrix for binary classification problems. Adapted from \cite{alpaydin2020ml}.}
    \label{tab:confusionMatrix}
\end{table}
\begin{table}[h]
    \centering
    \begin{tabular}[t]{c c c c c c}
        \toprule
        error & accuracy & TPR & FPR & precision & recall \\\midrule
        \((FP + FN) / N\) & \((TP + TN) / N \) & \(TP/p\) & \(FP/n\) & \(TP/p'\) & \(TP/p\)\\\bottomrule
    \end{tabular}
    \caption{Performance metrics for binary classification problems. Adapted from \cite{alpaydin2020ml}.}
    \label{tab:perfMetricsBinary}
\end{table}

Depending on the classification task, different metrics will be needed and derived from the confusion matrix. A non-exhaustive table of metrics is shown in Table \ref{tab:perfMetricsBinary}. Generally, if both classes are (approximately) equally represented, accuracy is good enough. However, for imbalanced datasets, precision and recall are usually reported in tandem. Similarly, true positive rates (TPR) and False positive rates (FPR) are also useful when the cost of
false positives is very high.

In this thesis, we make heavy use of two very interrelated metrics built on TPR and FPR, called the \textit{receiver operating characteristics} (ROC) graph and \textit{area under curve} (AUC), which are quite useful when the class distributions are skewed or when unequal classification error costs are present \cite{fawcett2006rocintro}. 

\begin{figure}
    \centering
    \begin{subfigure}{0.45\textwidth}
        \includegraphics[width=\textwidth]{./img/roc}
        \caption{ROC graph showing five different binary classifiers. Taken from \cite{fawcett2006rocintro}.}
        \label{fig:rocGraph}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
        \includegraphics[width=\textwidth]{./img/roc-curve}
        \caption{ROC curve created by thresholding a test set with 20 examples. Each point is labeled by the threshold which produces it. Taken from \cite{fawcett2006rocintro}.}
        \label{fig:rocCurve}
    \end{subfigure}
    \caption{ROC graph with five classifiers (left) and a ROC curve on a test set with 20 examples (right). Both images are taken from \cite{fawcett2006rocintro}.}
    \label{fig:rocBoth}
\end{figure}

Fawcett defines the ROC graph in the following way: ``ROC graphs are two-dimensional graphs in which \textit{tp rate} is plotted on the \(Y\) axis and \textit{fp rate} is plotted on the \(X\) axis. An ROC graph depicts relative tradeoffs between benefits (true positives) and costs (false positives)'' \cite{fawcett2006rocintro}. In practice, they are generally created by taking a whole range of classification thresholds (the score which determines whether an example will be classified as a
positive or a negative), and plotting their respective true positive and false positive rates as a curve (Figure \ref{fig:rocCurve}). Figure \ref{fig:rocGraph} shows five different classifiers on a single graph. There, D is the perfect classifier with no false positives and a true positive rate of
1.0, B and E are symmetric when one's output labels are switched, and depending on the problem at hand, B or A might be better if D is not available. A is said to be more ``conservative'' than B since it is located in the left-hand side of the curve (it has a lower false positive rate, meaning it needs stronger evidence to make a positive classification), and the line \(y = x\) represents the baseline of random guessing \cite{fawcett2006rocintro}.

One sometimes desirable property of ROC curves is that they are ``insensitive to changes in class distribution'' \cite{fawcett2006rocintro}, which means that class skew does not affect the metrics as they only consider true positives and true negatives. Finally, since the curve itself checks many different thresholds, it is sometimes more practical to compute the area under the ROC curve (abbreviated as AUC) which gives us a simpler metric for classifier comparison \cite{fawcett2006rocintro}.

\section{Privacy Attacks Against Machine Learning Models}
\label{sec:privacyattacks}
The vast amount of available data is one of the main reasons for the recent success of machine learning \cite{bengio2021deep}. Since these datasets often contain sensitive information, many lines of recent work investigate the presence of privacy leakage in their training data \cite{hu2021survey}, that is, many researchers have shown that said models memorise information about their training data in way which make them vulnerable to multiple different, potential
privacy attacks \cite{hu2021survey, carlini2019secret, shokri2017mia, de2020overview}. In fact, some of the recent research shows that some amount of memorization might even be integral to the training procedure \cite{carlini2019secret} as well as necessary to achieve optimial generalisation \cite{feldman2020does, feldman2020neural}. In this section we present an overview of these attacks since they are related to this thesis.

\begin{description}[style=unboxed, leftmargin=0cm]
    \item[Model extraction attack] \cite{tramer2016stealing} is an attack in which the attacker's goal is to steal or reconstruct the target model given black-box access but no prior knowledge of an ML model's parameters or private training data. While the seminal paper is focused on parameter stealing only, subsequent work has also been done on concepts related to hyperparameter stealing,
        architecture extraction \cite{de2020overview, hu2021survey} and others. Even though the attack is not directly related to obtaining confidential training data, model parameters might also be considered confidential (in the same way trade secrets are confidential) when the model is offered as a service. Furthermore, this attack can also be used as ``a stepping stone for inferring information about the model's training dataset'' \cite{shokri2017mia}.

    \item[Model inversion attack] (also known as the attribute inference attack) \cite{fredrikson2015model} ``uses the output of a model and information about the non-sensitive features to infer sensitive features'' \cite{hu2021survey}. This kind of attack does not always lead to leakage of private information \cite{fredrikson2015model, mcsherry2016statistical}, more specifically, model inversion ``produces the average of the features which characterise an entire output
        class'' \cite{shokri2017mia}, which implies a breach of privacy only in
        the case of facial recognition classifiers, given that the output for each class is associated with one person only. The average of the features can then be semantically meaningful because they describe that person, i.e. an average of the training examples for each person can be extracted using this type of attack.

    \item[Membership inference attack] (MIA), first proposed by Shokri et al.~\cite{shokri2017mia}, is an attack in which the adversary ``aims to \textit{infer} members of the training dataset of a machine learning model'', and as such, ``they can raise severe privacy risks to individuals'' \cite{hu2021survey}. The seminal paper \cite{shokri2017mia} proposed the first MIAs on multiple classifiers, and they demonstrated that the attacker is able to identify whether a data record was used to train a target neural network classifier or not, based solely on its prediction vector for that data record. This led to many other studies investigating the membership inference attack in various settings and on various
        target models, such as regression \cite{gupta2021membership}, generative models \cite{hayes2017logan} and embedding models \cite{song2020information}, as well as a large amount of work investigating potential defenses \cite{hu2021survey}.

\end{description}

Regarding what even constitutes leakage in machine learning, it is important to note that ``a report by the National Institute of Standards and Technology (NIST) specifically mentions that a memberhsip inference attack determining that an individual was included in the dataset used to train the target model is considered a confidentiality violation'' \cite{hu2021survey, tabassi2019taxonomy}. Furthermore, other researchers \cite{veale2018algorithms} have mentioned that ``membership inference attacks on machine learning models increase the risks of being classified
as personal data under the General Data Protection Regulation (GDPR)'' \cite{gdpr, hu2021survey, veale2018algorithms}. Since the membership inference attack is the basis and the focus of our investigation, we proceed to give a more detailed account of the attack in the following section.

\section{Membership Inference Attack}
\label{sec:mia}
In their survey, Hu et al.~\cite{hu2021survey} give the following definition of the membership inference attack: ``Given an exact input \(\textbf{x}\) and access to the learned model \(f_\theta(\textbf{x})\), an attacker infers whether \(\textbf{x} \in \mathcal{D}_{train}\) or not \cite{hu2021survey}'', where \(\mathcal{D}_{train} = \{(\textbf{x}^t, y^t)\}_{n=1}^N\) is the training dataset and \(f_\theta(\textbf{x})\) is the trained target model with parameters \(\theta\) \cite{hu2021survey}.
To answer this question, the attacker might have access to differing amounts of information and resources which could be used to perform the MIA, and we discuss the different possibilities in the following sections.

\subsection{Adversarial Knowledge}
\label{ssec:knowledge}
The amount of information available to the attacker can significantly affect the attack preformance \cite{truex2019demystifying}, and the types of knowledge can be broadly separated into two kinds of adversarial knowledge. The first is knowledge of the training data, and the other is knowledge about the target model \cite{hu2021survey}.

Knowledge of the training data generally refers to the distribution of training data, and in most research, it is assumed that the distribution is somehow available to the attacker, in a way that the attacker's dataset and the training set are generally taken to be disjoint \cite{hu2021survey, shokri2017mia, truex2019demystifying,
irolla2019demystifying, song2021systematic}. In other words, the attacker is able to obtain a dataset of examples drawn from the same distribution as the target training set (but usually not the same examples). To implement this assumption in practice, the attacker can perform one of a number of data-synthesis methods \cite{shokri2017mia, truex2019demystifying} to obtain said examples. If the data distribution is already known, then data can be synthesised using the statistics-based synthesis method \cite{hu2021survey, shokri2017mia}. If the data distribution is not known,
then a hill-climbing algorithm is used to move a randomly generated example in the right direction of the input space by querying the target model and observing the prediction vector, which is known as the model-based synthesis \cite{shokri2017mia}.
The other type of knowledge is knowledge of the target model \cite{hu2021survey}, and it includes the target model's architecture, parameters, hyperparameters, gradients, training procedure \cite{hu2021survey} etc.

Depending on the amount of available data, the attack settings can be roughly categorised in the white-box setting and the black-box setting \cite{hu2021survey}. The white-box setting includes all the aforementioned information which could be useful to make the attack stronger. For example, some research has shown that the last two or three gradients in a neural network could make the attack more potent \cite{rezaei2021difficulty}, and this is the easiest setting for the
attacker.

Black-box setting, on the other hand, limits the attacker to the training data distribution (due to the feasibility of the assumption as explained above) and to queries on the target model \cite{hu2021survey}, that is, the attacker only receives prediction vectors for the queried input example. In this thesis, we assume a black-box setting for the attack.

\subsection{Shadow Training}
\label{ssec:shadow}

The seminal paper by Shokri et al.~\cite{shokri2017mia} proposes to turn the membership inference attack question into a binary classification problem where the adversary trains the attack model to distinguish between training and test examples using the target classifier's outputs, which could in both cases be vectors with high class probabilities. The intuition here is that there are subtle differences in outputs produced by the target models between examples which have been memorised and those which were not seen before.
It is important to note that the assumptions here represent a black-box setting, that is, the attacker is only able to query the model and at best has some data from the same distribution as the training set \cite{shokri2017mia, hu2021survey}.

Figure \ref{fig:miaPrvi} shows the general attack procedure \cite{shokri2017mia}. An adversary who has a labeled example \((\textbf{x}, y)\) queries the target model \(f_{target}\) and obtains the prediction vector \(f_{target}(\textbf{x}) = \textbf{y}\) which contains the probabilities that \(\textbf{x}\) belongs to each of the classes, which is then used by the attack model to predict whether the example \(\textbf{x}\) was present in the training dataset
\cite{shokri2017mia}.

\begin{figure}
    \centering
    \begin{subfigure}{0.45\textwidth}
        \includegraphics[width=\textwidth]{./img/shadow/slika1}
        \caption{The general procedure of the membership inference attack.}
        \label{fig:miaPrvi}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.45\textwidth}
        \includegraphics[width=\textwidth]{./img/shadow/slika2}
        \caption{Training the shadow models on their corresponding fractions of the shadow data.}
        \label{fig:miaShadow}
    \end{subfigure}
    \caption{Black-box membership inference attack (left) and shadow training (right). Both images from the seminal paper by Shokri et al.~\cite{shokri2017mia}.}
\end{figure}

The attack model is trained using the so-called \textit{shadow training} technique in which the attacker's goal is to replicate the behaviour of the target model on the training set's data distribution by training a number of models (\textit{shadow models}) on data from the same distribution (\textit{shadow data}, used to mimic the training data) \cite{shokri2017mia}. The shadow data is split into \(k\) smaller disjoint subsets, where \(k\) is the number of shadow models, and then
each of those subsets is split into a shadow training and shadow test set. The attacker uses these sets to train the shadow models, and since he knows whether an individual example was present during the training of the
shadow model or not, each example and its corresponding prediction vector (by the shadow model) are labeled as ``in'' or ``out'' accordingly \cite{shokri2017mia}. This gives the attacker direct insight into how various models behave on seen and unseen examples, and he can then make use of transferability to perform the attack \cite{truex2019demystifying}.

Figure \ref{fig:miaShadow} demonstrates the shadow training procedure. To better capture the target model's behaviour, shadow training and test sets come from the same distribution as the target training dataset, but they are all mutually disjoint, and the shadow models are trained independently \cite{shokri2017mia}. 

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{./img/shadow/slika3}
    \caption{Training the shadow models on their corresponding fractions of the shadow data. From the seminal paper by Shokri et al.~\cite{shokri2017mia}.}
    \label{fig:miaAttackTraining}
\end{figure}

Training the attack model is the final step before performing the actual attack. To this end, the attacker makes good use of the shadow models. Each shadow model generates the prediction vectors for its training and tests examples and additionaly labels them as ``in'' or ``out'' accordingly \cite{shokri2017mia}. These prediction vectors and the in/out labels are used as input vectors and ground truths respectively during the attack model training procedure (Figure \ref{fig:miaAttackTraining}).
In this way, the attacker now obtained an attack model which discriminates between predictions on training and test examples. To perform the attack, the attacker takes a candidate examplexamplerget model returns the probability vector for that sample, the attacker passes it to the attack model which infers its training set membership status \cite{shokri2017mia}.

While the seminal attack method proposes using \(k\) shadow models, where \(k\) is the number of target model's classes, other research has shown \cite{salem2018mlleaks, truex2019demystifying} that
even one shadow model is enough for a potent attack. It is the latter variant with only one shadow model which we use in this thesis.

\subsection{Threshold Attack}
\label{ssec:threshold}
The shadow training method which we just summarised originally proposed in the seminal paper by Shokri et al.~\cite{shokri2017mia}, has a number of assumptions which are generally hard to achieve \cite{salem2018mlleaks}. First and foremost, there is the assumption that the attacker is somehow able to obtain a fraction of the data from the same distribution as the training set to train the shadow models \cite{shokri2017mia, hu2021survey}, which is often extremely difficult to perform due to the
fact that the data synthetisation method requires a large amount of queries to sample
just a single data instance \cite{salem2018mlleaks, bai2021ganmia}. The second assumption is that the attacker either knows the architecture of the target model, or that the shadow models mimic the behaviour of the target model well
enough \cite{shokri2017mia, salem2018mlleaks}.

Salem et al.~\cite{salem2018mlleaks} relax both of these adversarial assumptions by introducing a new kind of a membership inference attack which does not require training of shadow models, proposing an attack which is much more transferable. Here, only the target model's posterior probabilities \(f_\theta(\textbf{x}_{target})\) are relied on after querying the target model. Unlike shadow training, the attack model is implemented as an unsupervised binary classifier. The adversary first
``queries the target model and obtains
the posterior probabilities for the candidate example \(\textbf{x}_{target}\), and then extracts the highest posterior probability and compares it to a preset threshold. If the probability is higher than the threshold, the target sample is classified as one present in the training set, and vice versa'', because ``an ML model is more confident, i.e., one posterior is much higher than others, when facing a data point that it was trained on. In other words, the
maximal posterior of a member data point is much higher than the one of a non-member data point'' \cite{salem2018mlleaks}.

The threshold can be chosen according to the attacker's requirements, for example by choosing a fairly high threshold when precision is maximised, or a low threshold when recall is the focus \cite{salem2018mlleaks}. There is also a general method for choosing a threshold, and it involves generating a sample of random points in the input space of the target data point (in their paper, they propose uniform distributions for pixels, binary features, for example). Then these
points are used as inputs to the target model to obtain their respective maximal posterior probabilities, and since the points were most likely not present in the training set, the top \(t\) percentile of these posteriors are proposed as a ``good'' threshold \cite{salem2018mlleaks}.

\section{Inference Attack Metrics}
\label{sec:metrics}
Until recently, most research papers dealing with the membership inference attacks have used accuracy, precision and recall as their main metrics \cite{irolla2019demystifying, rezaei2021difficulty, hu2021survey}, where ``precision is the fraction of the records inferred as members of the training set that are indeed members, and recall is a measure of the coverage of the attack, that is, the fraction of the training examples which the attacker can correctly infer as members'' \cite{shokri2017mia}. However, recent findings
\cite{irolla2019demystifying, rezaei2021difficulty, carlini2021membership} have shown that ``the membership attack metrics have failed to properly take into account the false positive rate'' \cite{rezaei2021difficulty}, and more recent papers use ROC and AUC \cite{ye2021enhanced, salem2018mlleaks} or TPR and FPR \cite{rezaei2021difficulty, carlini2021membership} instead. Therefore, we have also used AUC as our main metric to measure the success of the membership inference attack.

Additionally, we used another metric called the \textit{privacy risk} proposed by Song et al.~\cite{song2021systematic}. While accuracy, precision, recall and AUC are all \textit{global} or \textit{aggregate} metrics (which are averaged over all examples), their proposed method is a fine-grained way of measuring privacy risk. The following definition is proposed in their paper \cite{song2021systematic}:

``The privacy risk score of an input sample \(\textbf{z} = (\textbf{x}, y)\) for the target machine learning model \(f_\theta\) is defined as the posterior probability that it is from the training set \(\mathcal{D}_{tr}\) after observing the target model's behaviour over that sample denoted as \(O(f_\theta, \textbf{z})\)'', i.e.,

\begin{equation}
    r(\textbf{z}) = P(\textbf{z} \in \mathcal{D}_{tr}|O(f_\theta, \textbf{z}))
\end{equation}
which can be expanded using the Bayes theorem and computed in practice under the assumption that the prior probabilities that the samples are taken from training and test sets respectively are equal to 0.5.

\section{Differential Privacy}
\label{sec:dp}
Differential privacy is a mathematical framework which tries to provide plausible deniability to users whose data is being processed by a statistical algorithm. The intuition behind differential privacy is that including or excluding a single training example in a dataset should not result in significant changes in the output of the algorithm which processes that dataset, meaning that observing the results provided by the algorithm should not lead to leakage of private
information.

Abadi et al.~give the following definition of differential privacy:
``A randomised mechanism \(\mathcal{M}: \mathcal{D} \rightarrow \mathcal{R}\) with domain \(\mathcal{D}\) and range \(\mathcal{R}\) satisfies \((\varepsilon, \delta)\)-differential privacy if for any two adjacent inputs \(d, d' \in \mathcal{D}\) and for any subset of outputs \(S \subseteq \mathcal{R}\) it holds that:'' \cite{abadi2016deep}

\begin{equation}
    Pr[\mathcal{M}(d) \in S] \leq e^\varepsilon Pr[\mathcal{M}(d') \in S] + \delta.
\end{equation}

In that paper, they also propose a practical implementation of differential privacy for deep neural networks, which is a differentially private variant of the stochastic gradient descent (SGD). Regular SGD uses small batches of training examples to update the model weights in a way which minimises the loss function. On the other hand, DP-SGD performs some additional operations on the gradient before updating the weights. Specifically, it also clips the L2 norm of each gradient, computes the
average and then
applies a certain amount of noise to increase privacy protection of individual data \cite{abadi2016deep}.

It is important to note that there are many other existing attempts to mitigate privacy leakage in non-differentially private manners, including but not limited to restricting the output of the target model only to the predicted class instead of the entire probability vector \cite{shokri2017mia} and adversarial regularisation \cite{nasr2018advreg}, in which an attacking model is used in the training procedure of the target model. Specifically, the defender trains the target
model in a way which minimises loss and target success rates simultaneously \cite{nasr2018advreg}. However, both these methods are experimentally proven to be vulnerable as well \cite{song2021systematic, rahimian2020sampling}.
