\label{ch:methods}
Overfitting has been noted as the main, but not the only, driving factor for the membership inference attack \cite{shokri2017mia, truex2019demystifying, yeom2018privacy, rezaei2021difficulty}. This is, however, a fairly general statement. 
While we have already discussed the relationship between model capacity and overfitting and underfitting in Section \ref{sec:supervised}, in practice, there could be more causes for the imbalance between model capacity and the capacity required to optimally approximate the underlying function.

Other than just choosing an inappropriate hypothesis class, we might simply be working with insufficient training data. This insufficiency can either be general or in the case of class skew, we might be lacking data from a certain class more often than other classes. Another factor which might lead to overfitting is an inappropriate choice of hyperparameters during optimisation, such as training a neural network using too many epochs \cite{goodfellow2016deep}.
Finally, due to the incompleteness of training data, it might be the case that the model simply does not get to see all the features belonging to a certain class, which results in a shifted decision boundary leading to worse generalisation.

Many experiments were performed to answer this question in relation to the membership inference attack \cite{shokri2017mia, truex2019demystifying, yeom2018privacy, rezaei2021difficulty, irolla2019demystifying}, and we reproduce some of them to gain a better insight into how much each of the aforementioned factors affects the attack success. Specifically, Shokri et al.~\cite{shokri2017mia} train a neural network classifier on the Purchase100 dataset, and they use a machine learning
as a service classifier of unknown architecture for the UCI Adult dataset obtained by MLaaS. They perform the shadow training attack on the preset data fraction, and they also perform the attack on a CIFAR-10 classifier while varying the amount of training data. We reproduce their results by using the some of the datasets and a neural network architecture (named Purchase100 Small NN) which they have used, and we try to use a number of non-neural network based models as a replacement for their MLaaS model of unknown architecture.
Additionally, we also include the Purchase100 neural network classifier architecture used by Song et al.~\cite{song2021systematic} in our experiments (dubbed Purchase100 Large NN) to compare our results with theirs.

To test their attack which relaxes a number of assumptions (Section \ref{ssec:threshold}), Salem et al.~\cite{salem2018mlleaks} use it on the same set of datasets. We reproduce their results on Purchase100 and UCI Adult by including the threshold attack in our experiments, specifically those described in Sections \ref{ssec:purchaseArchitectures} and \ref{sec:experimentTrainingData}.

Rezaei et al.~\cite{rezaei2021difficulty} show that the MIA is much harder to use on correctly classified instances, and we replicate their experiment -- which includes observing the attack success on the data splits corresponding to correctly and incorrectly classified instances -- by using our aforementioned architectures and datasets which they have not used in their paper.

We also try to use PCA to correlate features to privacy risk, which was inspired by an experiment performed by Song et al.~\cite{song2021systematic}. Specifically, they used t-SNE to plot the classes with highest and lowest membership risks and observed the spread in the graph to explain the risk related to in-class variance. Here we take a step further by trying to correlate memberhsip risk with data features using PCA and its loadings matrix (Section \ref{ssec:Features} and
\ref{sec:experimentTrainingData}).

We also perform two more experiments, one which included simply plotting the decision borders on a number of non-NN models to observe whether there are any patterns in relation to privacy leakage (Section \ref{ssec:decisionBoundaryAnalysis}), and the other in which we tried to generate our own synthetic dataset using CMA-ES (Section \ref{sec:synthetic}).

\section{Datasets}
\label{sec:datasets}
Due to time constraints and the in-depth analysis performed, we have unfortunately not managed to experiment on more than the two datasets described below.

\begin{description}[style=unboxed, leftmargin=0cm]
    \item[Purchase100] This dataset is a derivative of Kaggle's ``Acquire Valued Shoppers Challenge" dataset\footnote{Kaggle Acquire Valued Shoppers Challenge: \url{https://www.kaggle.com/competitions/acquire-valued-shoppers-challenge/overview}} developed by Shokri et al. in their seminal MIA paper \cite{shokri2017mia}. While the original dataset includes many more features, examples in Purchase100 consist of 600 binary features, each denoting whether the sampled user purchased a product or not.
        Examples are then ``clustered
        into 100 classes representing different shopping styles, and the classification task is to predict the sampled user's shopping style given the 600-dimensional feature vector'' \cite{shokri2017mia}. Of the entire dataset, 25\% was taken and split in two halves (24665 examples each) where one half was used for training of the target models, and the other was used as their test set, as well as for training of the shadow models (with the exception of the experiment described in Section
        \ref{sec:experimentTrainingData}). The simplified Purchase100 dataset is available on the researchers' git repository \cite{shokri2017mia}.
    \item[UCI Adult] Also known as ``Census Income" dataset, this dataset includes 48842 examples with 14 binary and categorical features including age, sex, relationship and marital status, education, occupation and others. It was made for a binary classification task where the goal is to predict whether a person's income exceeds \$50K per year or not, based on the census attributes. We use 50\% of the examples as the target model's training set, and the remaining half as the test
        test as well as the shadow training set, yielding two sets with 24421 examples each. We provide a statistical description of this dataset in Appendix \ref{tab:adultDescription}. UCI Adult is available in the UCI Machine Learning Repository \cite{uciRepository} and can be downloaded directly from the website.
\end{description}

\section{Experimental Setup}
\label{sec:experimentSetup}
In the implementation of the experiments in this thesis, we use Keras \cite{chollet2015keras} with the TensorFlow \cite{tensorflow2015} backend for all the neural networks, while conventional machine learning models are trained and evaluated using the scikit-learn library \cite{scikit-learn}. For performing the membership inference attack and for evaluating the privacy risks of all the models, we use
the TensorFlow Privacy library \cite{andrew2019tensorflow}. For training the differentially private conventional models, we used IBM's Differential Privacy Library \cite{diffprivlib}.

In a similar vein to most of the previous research, in all of the experiments it is assumed that the attacker has a dataset drawn from the same distribution as that of the training dataset. This is modeled by simply training the shadow models on the test sets themselves. The exception to this assumption is the experiment described in Section \ref{sec:synthetic}, where the goal was to synthesize the shadow set as successfully as possible by querying the trained model.
The training sets and the test (shadow) sets are randomly selected from their respective datasets in a way that they are disjoint and are equally large.

In all our attacks, we used the threshold attack and shadow learning using k-NN and random forests. As a rule, we measured the attack success using AUC. In Section \ref{ssec:Features}, we also used membership probability \cite{song2021systematic} combined with the threshold attack against the entire dataset, in hope that we will gain better insight into the properties which make the membership inference attack possible.

In all our experiments, we would first train the models and obtain its predictions on training and test data. These would then be provided to the framework to train the attack model and AUCs and ROCs would be returned.

\subsection{Neural Network Architectures}
\label{ssec:purchaseArchitectures}
We have used Keras to implement two different neural network architectures for the Purchase100 dataset. The smaller network's architecture is a fully connected neural network with only one hidden layer of 128 neurons. The larger architecture is more complex. It is a fully connected neural network with layer sizes of 1024, 512, 256, 128. Both networks also include a 600-dimensional input layer and a 100-dimensional output layer. As the activation function, we used tanh in all the hidden layers and softmax in the
output layers. In both cases, we set the learning rate to 0.001 and the maximum number of epochs to 50.

The model architectures we chose are equivalent to those used in previous work which allows us a more accurate comparison of obtained results. Specifically, the smaller architecture was also used in the seminal work by Shokri et al. \cite{shokri2017mia}, while the larger one was used by Song et al. \cite{song2021systematic} in their respective experiments previously described.  All the networks were trained on a GeForce GTX 1060 6GB GPU.

\section{Dependency on Target Model Architecture and Optimization Methods}
\label{sec:experimentArchitecture}
We first tested how attack success depends on the target model architecture by trying out multiple neural network architectures and conventional machine learning models and then attacking them using the methods provided by the TensorFlow Privacy framework. Specifically, we trained the models and provided the prediction vectors and true labels for both the train and test (shadow) sets to the framework. This was done for both neural networks by varying the network size (Subsection
\ref{ssec:architectureNN}) and for conventional models (Subsection \ref{ssec:architectureAdult}). This experiment is a direct reproduction of one by \cite{shokri2017mia}.

\subsection{Neural Networks}
\label{ssec:architectureNN}
To test the impact of neural network size on the MIA, we trained both the small and the large neural networks on the Purchase100 dataset using the parameters stated in Subsection \ref{ssec:purchaseArchitectures}. Each of those architectures was trained using one of the three following optimization methods:
\begin{inparaenum}[(i)]
    \item RMSProp without early stopping,
    \item RMSProp with early stopping and 
    \item differentially private RMSProp without early stopping.
\end{inparaenum}
This gave us six models in total, which we summarise in Table \ref{tab:purchaseModels}. Once the models were trained, we performed the membership inference attack on each of them to measure the general attack performance across the entire dataset.

\begin{table}[ht]
    \centering
    \begin{tabular}{l l}
        \toprule
        Model & Optimization method \\\midrule
        & RMSProp \\\cmidrule{2-2}
        Small NN & RMSProp (Early Stopping) \\\cmidrule{2-2}
        & DP-RMSProp \\\midrule
        
        & RMSProp \\\cmidrule{2-2}
        Large NN & RMSProp (Early Stopping) \\\cmidrule{2-2}
        & DP-RMSProp \\
        \bottomrule
    \end{tabular}
    \caption{Purchase100 models and their optimization methods.}
    \label{tab:purchaseModels}
\end{table}

We include early stopping because research by Song et al.~\cite{song2021systematic} shows that it might be a good baseline for measuring privacy leakage in machine learning models. Since it is usually claimed that overfitting plays one of the biggest roles in the membership inference attack, and because it is also often used in practice to reduce the model generalization gap, it also serves as a natural starting point for defending from the attack. Therefore, we have also experimented by including and excluding early stopping in our
neural network training procedures.

\subsection{Impact of Machine Learning Model Type}
\label{ssec:architectureAdult}
Furthermore, we trained a number of different, conventional machine learning models on the Adult dataset. We used both the classical variants and the differentially private counterparts of random forests and logistic regression. At the time of writing, the library offers no differentially private implementations of the categorical naive Bayes or standalone decision trees, so for these, we only used the classical variants. Used models are summarized in Table
\ref{tab:adultmodels}.

\begin{table}[ht]
    \centering
    \begin{tabular}{l c c}
        \toprule
        Model & DP equivalent \\
        \midrule
        Logistic Regression & Yes \\
        Random Forest & Yes \\ \cmidrule{1-2}
        Categorical NB & No \\
%        Naive Bayes & 0.805 & 0.803 \\ \cmidrule{2-3}
%        DP Naive Bayes & 0.795 & 0.793 \\
%        \midrule
        Decision Tree & No \\
        \bottomrule
    \end{tabular}
    \caption{UCI Adult models.}
    \label{tab:adultmodels}
\end{table}

We trained the logistic regression and its differentially private equivalent using the ``lbfgs" solver and L2 regularization, and for the differentially private parameters we set \(epsilon\) (determined experimentally in Appendix \ref{ch:appendixB}) to 10.0 and \(data\_norm\) to 100. We used 100 estimators for both random forests, while naive Bayes and decision trees were used with the default settings.

\section{Dependency on Overfitting}
\label{sec:experimentOverfitting}
One of the main findings in previous research is that the membership influence attack success rates are heavily influenced by overfitting \cite{shokri2017mia, truex2019demystifying, irolla2019demystifying, yeom2018privacy}, and the main intuition behind this is in the hypothesized role of the of the shadow models, which is ``to characterize how the target model may be impacted by the inclusion of a particular instance, that is, how the decision boundary of the target
model may reveal the inclusion of an instance'' \cite{truex2019demystifying}. Therefore, we have trained the two Purchase100 neural networks from Section \ref{ssec:architectureNN} while varying training data sizes, with the assumption that more training data will help the model generalize better, thus leading to lower risks of privacy leakage. In Section \ref{ssec:trainingDataSize} we perform the attack on the entire dataset and try to correlate the attack success with the amount of training data
used and with generalization gaps.

In Section \ref{ssec:correctIncorrect} we then split up training and test data into two fractions containing only correctly and incorrectly classified instances and observe the attack results on these two disjoint sets separately to conclude whether there is a pattern in how successful the attack is on seen vs. unseen instances compared to correctly vs. incorrectly classified ones. Here, we again use the four differently-sized training and test data fractions on the large
Purchase100 target model, and we also try the attack on all of the UCI Adult models with the original data splits.

Additionally, in Section \ref{ssec:decisionBoundaryAnalysis} we also plotted the decision boundaries of the UCI Adult decision tree, logistic regression and two random forests with varying amounts of estimators to determine whether it is possible to
learn something more about the membership inference attack from the way decision boundaries are formed.

\section{Dependency on Training Data Composition}
\label{sec:experimentTrainingData}
Other than model architectures and their optimization methods, previous research suggested that the success of membership inference attacks is also determined by the dimensionality of the classification problem -- where classifiers which had to discriminate between more classes were more vulnerable than binary classifiers in some cases -- as well as the data itself \cite{shokri2017mia, truex2019demystifying}.

In this case, the intuition is that increasing the number of classes partitions the input space into more smaller regions around instances which represent them. This in turn means that an individual instance has more impact on the shape of the decision boundary, which results in a higher likelihood of membership inference for those instances \cite{truex2019demystifying}. 

Under the assumption that the higher amount of training data teaches the model to generalize better and prevents the described mechanism, we ran the membership inference attack across all Purchase100 and Adult classes separately on all trained models to analise the connection between class training sizes and attack success.

Furthermore, we also tried to correlate privacy risk to individual features of the UCI Adult dataset. To this end, after performing the membership inference attack and computing the membership probability risks using the threshold attack, we scaled the data using scikit-learn's StandardScaler and transformed the examples to a two-dimensional plane using PCA, hoping to find patterns in regions with data of lowest and highest membership privacy risk. We then looked at PCA loadings
and the loadings matrix to determine the relationship between privacy risk and original features.
